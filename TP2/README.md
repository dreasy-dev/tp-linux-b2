# TP2 : Gestion de service


# Sommaire

- [TP2 : Gestion de service](#tp2--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro blabla](#1-intro-blabla)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiquée par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom
- [x] SELinux désactivé (vérifiez avec `sestatus`, voir [mémo install VM tout en bas](https://gitlab.com/it4lik/b2-reseau-2022/-/blob/main/cours/memo/install_vm.md#4-pr%C3%A9parer-la-vm-au-clonage))

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web |

🌞 **Installer le serveur Apache**

- paquet `httpd`

            [dreasy@web ~]$ sudo dnf install -y httpd
             Complete!
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`



🌞 **Démarrer le service Apache**


  - faites en sorte qu'Apache démarre automatique au démarrage de la machine

            [dreasy@web ~]$ sudo systemctl enable httpd
            Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
            [dreasy@web ~]$ sudo systemctl start httpd

  - ouvrez le port firewall nécessaire

                [dreasy@web ~]$ sudo firewall-cmd  --add-port=80/tcp --permanent
                success
                [dreasy@web ~]$ sudo firewall-cmd  --reload
                success
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache

                [dreasy@web ~]$ ss -lr |grep http
                u_str LISTEN 0      100                   /etc/httpd/run/cgisock.1618 21858                         * 0
                tcp   LISTEN 0      511                                             *:http
                [dreasy@web ~]$ ss -lrn |grep 80
            
                tcp   LISTEN 0      511                                             *:80                     *:*



🌞 **TEST**



            [dreasy@web ~]$ curl localhost
            <!doctype html>
            <html>
            <head>
                <meta charset='utf-8'>
                <meta name='viewport' content='width=device-width, initial-scale=1'>
                <title>HTTP Server Test Page powered by: Rocky Linux</title>
                <style type="text/css">
                /*<![CDATA[*/
                [...]


## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

            [dreasy@web ~]$ sudo cat /etc/systemd/system/multi-user.target.wants/httpd.service
            [sudo] password for dreasy:
            # See httpd.service(8) for more information on using the httpd service.


            [Unit]
            Description=The Apache HTTP Server
            Wants=httpd-init.service
            After=network.target remote-fs.target nss-lookup.target httpd-init.service
            Documentation=man:httpd.service(8)

            [Service]
            Type=notify
            Environment=LANG=C

            ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
            ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
            # Send SIGWINCH for graceful stop
            KillSignal=SIGWINCH
            KillMode=mixed
            PrivateTmp=true
            OOMPolicy=continue

            [Install]
            WantedBy=multi-user.target

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**



                [dreasy@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
                [...]
                User apache
                Group apache
                [...]
            

                        [dreasy@web ~]$ ps -ef | grep httpd
                        root        1618       1  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                        apache      1619    1618  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                        apache      1620    1618  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                        apache      1621    1618  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                        apache      1622    1618  0 09:45 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                        dreasy      2011    1242  0 10:02 pts/0    00:00:00 grep --color=auto httpd

- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`

                [dreasy@web ~]$ ls -al /usr/share/testpage/
                total 12
                drwxr-xr-x.  2 root root   24 Nov 15 09:41 .
                drwxr-xr-x. 77 root root 4096 Nov 15 09:41 ..
                -rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html

la on dirait que rien n'est accessible par apache, mais enfaite c'est sous l'user root que le process apache est lancé donc il prend aussi ses permissions donc il peux lire le contenus de ses fichiers car apache est lancé par root 

🌞 **Changer l'utilisateur utilisé par Apache**

- le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine

                    [dreasy@web home]$ sudo cat /etc/passwd
                    [...]
                    httpapy:x:55555:55555:httpapy:/usr/share/httpd:/sbin/nologin

  
- utilisez une commande `ps` pour vérifier que le changement a pris effet
                [dreasy@web ~]$ ps -ef |grep http
                root         742       1  0 10:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                httpapy      757     742  0 10:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                httpapy      758     742  0 10:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                httpapy      760     742  0 10:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
                httpapy      761     742  0 10:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix

            [dreasy@web ~]$ sudo cat /etc/httpd/conf/httpd.conf

            ServerRoot "/etc/httpd"

            Listen 404
            [...]

- ouvrez ce nouveau port dans le firewall, et fermez l'ancien

                [dreasy@web ~]$ sudo firewall-cmd  --add-port=404/tcp --permanent
                success
                [dreasy@web ~]$ sudo firewall-cmd  --remove-port=80/tcp --permanent
                success
                [dreasy@web ~]$ sudo firewall-cmd  --reload
                success
                
- redémarrez Apache
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi

                    [dreasy@web ~]$ ss -lpn
                    tcp       LISTEN     0          511                                                     *:404                            *:*   

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port       

            [dreasy@web ~]$ curl localhost:404
            <!doctype html>
            <html>
            <head>
                <meta charset='utf-8'>
                <meta name='viewport' content='width=device-width, initial-scale=1'>
                <title>HTTP Server Test Page powered by: Rocky Linux</title>
                <style type="text/css">
                /*<![CDATA[*/



📁 **Fichier `/etc/httpd/conf/httpd.conf`**
                ***[Ici la](../files/httpd.conf)***

# II. Une stack web plus avancée


## 1. Intro blabla



## 2. Setup

🖥️ **VM db.tp2.linux**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

| Machines        | IP            | Service                 |
|-----------------|---------------|-------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données |

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**


- je veux dans le rendu **toutes** les commandes réalisées
[juste la](../files//mariadb_install.sh)
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

            [dreasy@db ~]$ ss -lnpt
            State         Recv-Q        Send-Q               Local LISTEN        0             80                               *:3306                          *:*
            [dreasy@db ~]$                         *:*                   *:*

  - il sera nécessaire de l'ouvrir dans le firewall

                    [dreasy@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
                    success
                    [dreasy@db ~]$ sudo firewall-cmd --add-port=3306/udp --permanent
                    success
                    [dreasy@db ~]$ sudo firewall-cmd --reload
                    success

cer la base qui a une configuration un peu *chillax* à l'install.

🌞 **Préparation de la base pour NextCloud**



🌞 **Exploration de la base de données**

- 
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`

                [dreasy@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
                Enter password:
                Welcome to the MySQL monitor.  Commands end with ; or \g.
                Your MySQL connection id is 11
                

                mysql> SHOW DATABASES;
                +--------------------+
                | Database           |
                +--------------------+
                | information_schema |
                | nextcloud          |
                +--------------------+
                2 rows in set (0.00 sec)

                mysql> USE nextcloud
                Database changed
                mysql> SHOW TABLES;
                Empty set (0.01 sec)
                ;
                ```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**


                [dreasy@web ~]$ mysql -u root -h 10.102.1.12 -p
                Enter password:
                Welcome to the MySQL monitor.  Commands end with ; or \g.

                mysql> SELECT User, Host FROM mysql.user;
                +-------------+-------------+
                | User        | Host        |
                +-------------+-------------+
                | nextcloud   | 10.102.1.11 |
                | root        | 10.102.1.11 |
                | roott       | 10.102.1.11 |
                | mariadb.sys | localhost   |
                | mysql       | localhost   |
                | root        | localhost   |
                +-------------+-------------+
                6 rows in set (0.00 sec)




### B. Serveur Web et NextCloud

🌞 **Install de PHP**

C'EST FAIT WHALLAH

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
Complete!
```

🌞 **Récupérer NextCloud**


                [dreasy@web tp2_nextcloud]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip -o nextcloud.zip

                [dreasy@web tp2_nextcloud]$ ls
                nextcloud  nextcloud.zip


            [dreasy@web tp2_nextcloud]$ ls |grep index
            index.html
            index.php

- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache

            [dreasy@web www]$ ls -al
            total 8
            drwxr-xr-x.  5 root root   54 Nov 15 11:27 .
            drwxr-xr-x. 15 root root 4096 Nov 15 11:35 tp2_nextcloud
            [dreasy@web www]$ sudo chown --recursive apache:apache tp2_nextcloud/
            [dreasy@web www]$ ls -al
            total 8
            drwxr-xr-x.  5 apache apache   54 Nov 15 11:27 .
            drwxr-xr-x. 15 apache apache 4096 Nov 15 11:35 tp2_nextcloud

🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf

            IncludeOptional conf.d/*.conf
- créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :

            [dreasy@web ~]$ sudo nano /etc/httpd/conf.d/nextcloud.conf
              <VirtualHost *:80>
                DocumentRoot /var/www/tp2_nextcloud/
                ServerName  web.tp2.linux
                <Directory /var/www/tp2_nextcloud/>
                  Require all granted
                  AllowOverride All
                  Options FollowSymLinks MultiViews
                  <IfModule mod_dav.c>
                    Dav off
                  </IfModule>
                </Directory>
              <VirtualHost>


🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf
    
                [dreasy@web ~]$ sudo systemctl restart httpd
                [dreasy@web ~]$ sudo systemctl status httpd
                ● httpd.service - The Apache HTTP Server
                    Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
                    Drop-In: /usr/lib/systemd/system/httpd.service.d
                            └─php81-php-fpm.conf
                    Active: active (running) since Tue 2022-11-15 11:49:15 CET; 9s ago
### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

                   10.102.1.11     web.tp2.linux

    tout fonctionne bien et j'ai pu me login apres une pitite erreur de conf

🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)
ca marche bien x) 

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
            | column_stats              |
            | columns_priv              |
            | db                        |
            | event                     |
            | func                      |
            | general_log               |
            | global_priv               |
            | gtid_slave_pos            |
            | help_category             |
            | help_keyword              |
            | help_relation             |
            | help_topic                |
            | index_stats               |
            | innodb_index_stats        |
            | innodb_table_stats        |
            | plugin                    |
            | proc                      |
            | procs_priv                |
            | proxies_priv              |
            | roles_mapping             |
            | servers                   |
            | slow_log                  |
            | table_stats               |
            | tables_priv               |
            | time_zone                 |
            | time_zone_leap_second     |
            | time_zone_name            |
            | time_zone_transition      |
            | time_zone_transition_type |
            | transaction_registry      |
            | user                      |
            +---------------------------+
            31 rows in set (0.001 sec)

  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL