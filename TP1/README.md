# TP1 : (re)Familiaration avec un système GNU/Linux


## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [1. Préparation de la VM](#1-préparation-de-la-vm)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine



🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**
  
```
  [dreasy@node1 ~]$ ping google.com
    PING google.com (142.250.74.238) 56(84) bytes of data.
    64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=114 time=24.3 ms
    64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=114 time=24.3 ms
```
```
    [dreasy@node2 ~]$ ping google.com
    PING google.com (216.58.215.46) 56(84) bytes of data.
    64 bytes from par21s17-in-f14.1e100.net (216.58.215.46): icmp_seq=1 ttl=114 time=23.6 ms
    64 bytes from par21s17-in-f14.1e100.net (216.58.215.46): icmp_seq=2 ttl=114 time=35.7 ms
```

- **un accès à un réseau local** (les deux machines peuvent se `ping`) (via la carte Host-Only)
  ```
    [dreasy@node1 ~]$ ip a | grep enp0s8
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
    ```
  ```
    [dreasy@node2 ~]$ ip a | grep enp0s8
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
  ```

- **vous n'utilisez QUE `ssh` pour administrer les machines**

- **les machines doivent avoir un nom**
  
  ```
     [dreasy@node1 ~]$ sudo nano /etc/hostname
     node1
  ```

  ```
     [dreasy@node2 ~]$ sudo nano /etc/hostname
     node2
  ```
  

- **utiliser `1.1.1.1` comme serveur DNS**
  
    ```
    [dreasy@node1 ~]$ dig ynov.com

    ;; ANSWER SECTION:
    ynov.com.               300     IN      A       104.26.10.233
    ynov.com.               300     IN      A       172.67.74.226
    ynov.com.               300     IN      A       104.26.11.233

    ;; Query time: 41 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)

    ```
    ```
    [dreasy@node2 ~]$ dig ynov.com


    ;; ANSWER SECTION:
    ynov.com.               300     IN      A       104.26.11.233
    ynov.com.               300     IN      A       172.67.74.226
    ynov.com.               300     IN      A       104.26.10.233

    ;; Query time: 27 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
   
    ```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**
 
```
    [dreasy@node2 ~]$ ping node1
PING node1 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.676 ms
64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.905 ms
```




| Name               | IP            |
|--------------------|---------------|
| 🖥️ `node1.tp1.b2` | `10.101.1.11` |
| 🖥️ `node2.tp1.b2` | `10.101.1.12` |
| Votre hôte         | `10.101.1.1`  |

## I. Utilisateurs



### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**, qui sera dédié à son administration

- 

            [dreasy@node1 ~]$ sudo useradd admin -m -s /bin/bash -u 55555 -p root
            [dreasy@node1 ~]$ sudo passwd admin
            root
            root


            [dreasy@node1 home]$ ls
            admin  dreasy

🌞 **Créer un nouveau groupe `admins`** qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

            [dreasy@node1 home]$ sudo groupadd admins



            [dreasy@node1 home]$ sudo visudo


           [dreasy@node1 home]$ sudo cat etc/sudoers
            ## Allows people in group wheel to run all commands
        %wheel  ALL=(ALL)       ALL
        %admins ALL=(ALL)       ALL

🌞 **Ajouter votre utilisateur à ce groupe `admins`**



            [admin@node1 ~]$ sudo -l
            User admin may run the following commands on node1:
                (ALL) ALL

---

1. Utilisateur créé et configuré
2. Groupe `admins` créé
3. Groupe `admins` ajouté au fichier `/etc/sudoers`
4. Ajout de l'utilisateur au groupe `admins`

### 2. SSH

[Une section dédiée aux clés SSH existe dans le cours.](../../cours/SSH/README.md)



🌞 **Pour cela...**
🌞 **Assurez vous que la connexion SSH est fonctionnelle**, sans avoir besoin de mot de passe.

            Connection to 10.101.1.11 closed.
            PS C:\Users\Dreasy> ssh dreasy@10.101.1.11
            Last login: Mon Nov 14 12:07:33 2022 from 10.101.1.1
            [dreasy@node1 ~]$
 


## II. Partitionnement

[Il existe une section dédiée au partitionnement dans le cours](../../cours/part/)

### 1. Préparation de la VM

⚠️ **Uniquement sur `node1.tp1.b2`.**

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

### 2. Partitionnement

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Utilisez LVM** pour...

            [dreasy@node1 ~]$ lsblk |grep disk
        sda           8:0    0   20G  0 disk
        sdb           8:16   0    3G  0 disk
        sdc           8:32   0    3G  0 disk

         [dreasy@node1 ~]$ sudo pvcreate /dev/sdb
            [sudo] password for dreasy:
            Physical volume "/dev/sdb" successfully created.
            [dreasy@node1 ~]$ sudo pvcreate /dev/sdc
            Physical volume "/dev/sdc" successfully created.

- agréger les deux disques en un seul *volume group*

             [dreasy@node1 ~]$ sudo vgdisplay disks
                Devices file sys_wwid t10.
                --- Volume group ---
                VG Name               disks
                Format                lvm2
                Metadata Areas        2
                Metadata Sequence No  2
                VG Access             read/write
                VG Status             resizable
                Act PV                2
                VG Size               5.99 GiB
                Free  PE / Size       1534 / 5.99 GiB

- créer 3 *logical volumes* de 1 Go chacun

                [dreasy@node1 ~]$ sudo lvcreate -L 1G disks -n data1
                Rounding up size to full physical extent 4.00 MiB
                Logical volume "data1" created.

            [dreasy@node1 ~]$ sudo lvs
            
            LV    VG    Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
            data1 disks -wi-a----- 1.00g
            data2 disks -wi-a----- 1.00g
            data3 disks -wi-a----- 1.00g
            
- formater ces partitions en `ext4`

             [dreasy@node1 ~]$ sudo  mkfs -t ext4 /dev/disks/data1
             [dreasy@node1 ~]$ sudo  mkfs -t ext4 /dev/disks/data2
             [dreasy@node1 ~]$ sudo  mkfs -t ext4 /dev/disks/data3

- monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1`, `/mnt/part2` et `/mnt/part3`.

                [dreasy@node1 ~]$ sudo mkdir /mnt/data1
                [dreasy@node1 ~]$ sudo mount /dev/disks/data1 /mnt/data1/

               [dreasy@node1 ~]$ df - h /dev/mapper/disks-data1  974M   24K  907M   1% /mnt/data1
                /dev/mapper/disks-data2  974M   24K  907M   1% /mnt/data2
                /dev/mapper/disks-data3  974M   24K  907M   1% /mnt/data3

🌞 **Grâce au fichier `/etc/fstab`**, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

            /dev/disks/data1        /mnt/data1              ext4    defaults        0 0
            /dev/disks/data2        /mnt/data2              ext4    defaults        0 0
            /dev/disks/data3        /mnt/data3              ext4    defaults        0 0



## III. Gestion de services



**Référez-vous au mémo pour voir les autres commandes `systemctl` usuelles.**

## 1. Interaction avec un service existant

⚠️ **Uniquement sur `node1.tp1.b2`.**



🌞 **Assurez-vous que...**

- l'unité est démarrée

            [dreasy@node1 ~]$ sudo systemctl status firewalld
            ● firewalld.service - firewalld - dynamic firewall daemon
                Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
                Active: active (running) since Mon 2022-11-14 12:13:21 CET; 26min ago
                Docs: man:firewalld(1)
            Main PID: 688 (firewalld)
                Tasks: 2 (limit: 14323)
                Memory: 41.4M
                    CPU: 669ms
                CGroup: /system.slice/firewalld.service
                        └─688 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid

- l'unitée est activée (elle se lance automatiquement au démarrage)

Le bloc du dessus est le resultat de `sudo systemctl status firewalld` le petit `enabled ` a la fin de 3eme ligne signifie que le serve demarre au lancement de l'os.

## 2. Création de service



### A. Unité simpliste

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Créer un fichier qui définit une unité de service** 



            [dreasy@node1 ~]$ sudo nano /etc/systemd/system/web.service

```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

Le but de cette unité est de lancer un serveur web sur le port 8888 de la machine. **N'oubliez pas d'ouvrir ce port dans le firewall.**

            [dreasy@node1 ~]$ sudo firewall-cmd --zone=public --permanent --add-port 8888/tcp
success

Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :

```
[dreasy@node1 ~]$ sudo systemctl status web
● web.service
     Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-14 12:47:02 CET; 8s ago
   Main PID: 1630 (python3)
      Tasks: 1 (limit: 14323)
     Memory: 9.2M
        CPU: 82ms
     CGroup: /system.slice/web.service
             └─1630 /bin/python3 -m http.server 8888

```
        ne pas oublier de reload le firewall x)
🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**



            [dreasy@node2 ~]$ curl 10.101.1.11:8888
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <title>Directory listing for /</title>
            </head>
            <body>
            <h1>Directory listing for /</h1>
            <hr>
            <ul>
            <li><a href="afs/">afs/</a></li>
            <li><a href="bin/">bin@</a></li>
            <li><a href="boot/">boot/</a></li>
            <li><a href="dev/">dev/</a></li>
            <li><a href="etc/">etc/</a></li>
            <li><a href="home/">home/</a></li>
            <li><a href="lib/">lib@</a></li>
            <li><a href="lib64/">lib64@</a></li>
            <li><a href="media/">media/</a></li>
            <li><a href="mnt/">mnt/</a></li>
            <li><a href="opt/">opt/</a></li>
            <li><a href="proc/">proc/</a></li>
            <li><a href="root/">root/</a></li>
            <li><a href="run/">run/</a></li>
            <li><a href="sbin/">sbin@</a></li>
            <li><a href="srv/">srv/</a></li>
            <li><a href="sys/">sys/</a></li>
            <li><a href="tmp/">tmp/</a></li>
            <li><a href="usr/">usr/</a></li>
            <li><a href="var/">var/</a></li>
            </ul>
            <hr>
            </body>
            </html>

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

- créer un utilisateur `web`

        [dreasy@node1 ~]$ sudo useradd web -u 8888
- créer un dossier `/var/www/meow/`g

            [dreasy@node1 meow]$ ls -l
            total 8
            -rw-r--r--. 1 root root 5 Nov 14 12:56 test.sh

> Pour que tout fonctionne correctement, il faudra veiller à ce que le dossier et le fichier appartiennent à l'utilisateur `web` et qu'il ait des droits suffisants dessus.

            [dreasy@node1 www]$ sudo chown web meow/
            [dreasy@node1 www]$ ls -al
                total 4
               
                drwxr-xr-x.  2 web  root   37 Nov 14 12:56 meow

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**


            [dreasy@node1 system]$ cat web.service
            #web service whoulah

            [Unit]
            Description=Very simple web service

            [Service]
            ExecStart=/bin/python3 -m http.server 8888
            User=web
            WorkingDirectory=/var/www/meow

            [Install]
            WantedBy=multi-user.target

🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**

            [dreasy@node2 ~]$ curl 10.101.1.11:8888
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Directory listing for /</title>
                </head>
                <body>
                <h1>Directory listing for /</h1>
                <hr>
                <ul>
                <li><a href="test.sh">test.sh</a></li>
                <li><a href="test.txt">test.txt</a></li>
                </ul>
                <hr>
                </body>
                </html>

et la on voit bien mes deux fichiers debiles qui prouve que nous somme dans le /var/www/meow