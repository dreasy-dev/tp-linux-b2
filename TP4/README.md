# TP4 : Conteneurs

# Sommaire

- [TP4 : Conteneurs](#tp4--conteneurs)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Docker](#i-docker)
  - [1. Install](#1-install)
  - [2. Vérifier l'install](#2-vérifier-linstall)
  - [3. Lancement de conteneurs](#3-lancement-de-conteneurs)
- [II. Images](#ii-images)
- [III. `docker-compose`](#iii-docker-compose)
  - [1. Intro](#1-intro)
  - [2. Make your own meow](#2-make-your-own-meow)

# 0. Prérequis


## Checklist


**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/engine/install/)
- démarrer le service `docker` avec une commande `systemctl`
- ajouter votre utilisateur au groupe `docker`
  - cela permet d'utiliser Docker sans avoir besoin de l'identité de `root`
  - avec la commande : `sudo usermod -aG docker $(whoami)`
  - déconnectez-vous puis relancez une session pour que le changement prenne effet
    
        [dreasy@localhost share]$ sudo usermod -aG docker $(whoami)
        [dreasy@localhost]$ docker ps

## 2. Vérifier l'install

➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**


➜ **Explorer un peu le help**, si c'est pas le man :


## 3. Lancement de conteneurs


🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`
  - l'app NGINX doit avoir un fichier de conf personnalisé [la](./test.conf)
  - l'app NGINX doit servir un fichier `index.html` [juste ici](./index.html) personnalisé

  - l'application doit être joignable grâce à un partage de ports
  - vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
  - le conteneur devra avoir un nom


             docker run -p 443:443 --name nginx -m="2g" --cpus="1.0" -v /usr/share/index.html:/usr/share/nginx/html/index.html:ro -v /etc/nginx/conf.d/test.conf:/etc/nginx/conf.d/toto.conf nginx:1.22

> Tout se fait avec des options de la commande `docker run`.

# II. Images


## 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

- image de base (celle que vous voulez : debian, alpine, ubuntu, etc.)
  - une image du Docker Hub
  - qui ne porte aucune application par défaut
- vous ajouterez
  - mise à jour du système
  - installation de Apache [Get ca](./Dockerfile)
  - page d'accueil Apache HTML personnalisée [index.html](./index.html)

            gamin@Dreasy MINGW64 ~
            $ curl http://10.104.1.5:8884/
            % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                            Dload  Upload   Total   Spent    Left  Speed
            100    28  100    28    0     0  12873      0 --:--:-- --:--:-- --:--:-- 28000
            coucou c'est pas moi encore



📁 **`Dockerfile`**

# III. `docker-compose`

## 1. Intro

➜ **Installer `docker-compose` sur la machine**

## 2. Make your own meow

Pour cette partie, vous utiliserez une application à vous que vous avez sous la main.

N'importe quelle app fera le taff, un truc dév en cours, en temps perso, au taff, peu importe.

Peu importe le langage aussi ! Go, Python, PHP (désolé des gros mots), Node (j'ai déjà dit désolé pour les gros mots ?), ou autres.

🌞 **Conteneurisez votre application**

- créer un `Dockerfile` maison qui porte l'application (DockerFile)[./app/DockerFile]
- créer un `docker-compose.yml` qui permet de lancer votre application (docker-compose.yml)[./app/docker-compose.yml]
- vous préciserez dans le rendu les instructions pour lancer l'application
  - indiquer la commande `git clone`
  - le `cd` dans le bon dossier
  - la commande `docker build` pour build l'image
  - la commande `docker-compose` pour lancer le(s) conteneur(s)

📁 📁 `app/Dockerfile` et `app/docker-compose.yml`. Je veux un sous-dossier `app/` sur votre dépôt git avec ces deux fichiers dedans :)