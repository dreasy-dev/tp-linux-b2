ip a

#install de MariaDB et MySQL
dnf install mariadb-server -y
systemctl enable mariadb
systemctl start mariadb
dnf module enable mariadb:10.5
firewall-cmd --add-port=3306/udp --permanent
firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --reload


#Setup de mysql
mysql_secure_installation


#status
systemctl status mariadb
