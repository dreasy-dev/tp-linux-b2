# Module 6 : Automatiser le déploiement

Bon bah là je vais pas beaucoup écrire pour ce sujet.

C'est simple : automatiser via un script `bash` le TP2. Tout ce que vous avez fait à la main dans le TP2 pour déployer un NextCloud et sa base de données, le but ici est de le faire de façon automatisée *via* un script `bash`.

➜ **Ecrire le script `bash`**

➜ **Tester le script `bash`**

[Juste la](./tp3_automatisation_nextcloud.sh)

pour l'installer sinon depuis la machine : 
    
    ```bash 
    sudo curl https://gitlab.com/dreasy-dev/tp-linux-b2/-/raw/main/TP3/3/tp3_automatisation_nextcloud.sh -o /usr/tp3_automatisation_nextcloud.sh
    sudo chmod +x /usrtp3_automatisation_nextcloud.sh
    sudo bash /usr/tp3_automatisation_nextcloud.sh
    
    ```

    Et hop finito pour la partie nextcloud


✨ **Bonus** : faire un deuxième script `tp3_automation_db.sh` qui automatise l'installation de la base de données de NextCloud

[Juste la](./tp3_automatisationDB.sh)

Pour lui je suis un peu moins sur de la procedure mais globallement le system est le meme que pour le nextcloud

    ```bash 
    sudo curl https://gitlab.com/dreasy-dev/tp-linux-b2/-/raw/main/TP3/3/tp3_automatisationDB.sh -o /usr/tp3_automatisationDB.sh
    sudo chmod +x /usr/tp3_automatisationDB.sh
    sudo bash /usr/tp3_automatisationDB.sh
    
    ```
