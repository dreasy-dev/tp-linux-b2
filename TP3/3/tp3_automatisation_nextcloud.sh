##Automatisation de l'installation de Nextcloud, apache et 
#configuration general des soft et du firewall

#Mise a jour du systeme
dnf update -y
dnf upgrade -y

#Installation de apache
dnf install httpd -y

#Configuration du firewall
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
firewall-cmd --reload

#Configuration de SELinux
setenforce 0

#Installation de php

curl https://rpmfind.net/linux/fedora/linux/updates/35/Everything/x86_64/Packages/l/libdav1d-0.9.2-1.fc35.x86_64.rpm -SLO*
curl -SLO https://rpmfind.net/linux/fedora/linux/releases/37/Everything/x86_64/os/Packages/l/libjxl-0.7.0-3.fc37.x86_64.rpm
curl -SLO https://rpmfind.net/linux/opensuse/tumbleweed/repo/oss/x86_64/libhwy1-1.0.2-2.1.x86_64.rpm
rpm -ivh libhwy1-1.0.2-2.1.x86_64.rpm
rpm -ivh libjxl-0.7.0-3.fc37.x86_64.rpm
dnf install -y glibc.x86_64
rpm -q --whatprovides /usr/lib64/libm.so.6
rpm -ivh libjxl-0.7.0-3.fc37.x86_64.rpm
dnf search glibc    
dnf install -y glibc
dnf upgrade
dnf config-manager --set-enabled crb -y
dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
dnf module list php -y 
dnf module enable php:remi-8.1 -y
dnf install  php81-php -y 
dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp

#Configuration de apache
systemctl enable httpd
systemctl start httpd

#Installation de Nextcloud
mkdir /var/www/html/nextcloud
cd /var/www/html/nextcloud

curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip -o nextcloud.zip
dnf install unzip -y 
unzip nextcloud.zip
mv nextcloud/* .
mv nextcloud/.htaccess .
mv nextcloud/.user.ini .
rm -rf *nextcloud*
chown -R apache:apache /var/www/html/nextcloud
chmod -R 755 /var/www/html/nextcloud
cd /etc/httpd/conf.d/
curl "https://gitlab.com/dreasy-dev/tp-linux-b2/-/raw/main/TP3/3/nextcloud.conf" -o nextcloud.conf
cd
cd /etc/httpd/conf/
rm -rf httpd.conf
curl "https://gitlab.com/dreasy-dev/tp-linux-b2/-/raw/main/TP3/3/httpd.conf" -o httpd.conf
systemctl restart httpd
systemctl status httpd











