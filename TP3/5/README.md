# Module 7 - Fail2Ban

- [Module 7 : Fail2Ban](#module-7--fail2ban)
- [I. Installation de Fail2Ban](#i-installation-de-fail2ban)
- [II. Configuration de Fail2Ban](#ii-configuration-de-fail2ban)
- [III. Testing Zone](#iii-testing-zone)
    - [1. Time to get banned](#1-time-to-get-banned)
    - [2. L'évasion !](#2-lévasion-)

On mettra ici les détails de l'installation sur la machine `db.tp2.linux`, mais Fail2Ban a été par la suite installé sur toutes les autres machines pour protéger le SSH.

## I. Installation de Fail2Ban

- Installation du repository EPEL
    ```bash
    [dreasy@db ~]$ sudo dnf install epel-release -y
    Complete!
    ```
- Installation de Fail2Ban
    ```bash
    # Installation de Fail2Ban
    [dreasy@db ~]$ sudo dnf install fail2ban -y
    Complete!
    ```

## II. Configuration de Fail2Ban

- On copie la configuration comme demandé
    ```bash
    [dreasy@db ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

    # On peut maintenant modifier jail.local pour configurer Fail2Ban
    ```
- On modifie la configuration
    ```bash
    # Par défaut, Fail2Ban utilise IPtables, donc on modifie ça
    [dreasy@db ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
    [dreasy@db ~]$ sudo systemctl restart fail2ban

    # On crée une conf pour le ssh
    [dreasy@db ~]$ sudo nano /etc/fail2ban/jail.d/sshd.local
    [dreasy@db ~]$ cat /etc/fail2ban/jail.d/sshd.local
    [sshd]
    enabled = true
    bantime = 1d
    findtime = 1m
    maxretry = 3
    [dreasy@db ~]$ sudo systemctl restart fail2ban

    # On check les status
    [dreasy@db ~]$ sudo fail2ban-client status
    Status
    |- Number of jail:      1
    |- Jail list:   sshd

    # On peut aussi récupérer des propriétés en commandes
    [dreasy@db ~]$ sudo fail2ban-client get sshd maxretry
    3
    ```
- On lance et on active Fail2Ban
    ```bash
    # Lancement du service
    [dreasy@db ~]$ sudo systemctl start fail2ban

    # Activation du service
    [dreasy@db ~]$ sudo systemctl enable fail2ban
    Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
    ```

## III. Testing zone

### 1. Time to get banned

- On essaye de faire 3 erreurs de mots de passe depuis une autre machine
    ```bash
    [dreasy@web ~]$ ssh dreasy@10.102.1.12
    dreasy@10.102.1.12's password:
    Permission denied, please try again.
    dreasy@10.102.1.12's password:
    Permission denied, please try again.
    dreasy@10.102.1.12's password:
    dreasy@10.102.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
    [dreasy@web ~]$ ssh dreasy@10.102.1.12
    ssh: connect to host 10.102.1.12 port 22: Connection refused
    ```
- Fail2Ban affiche bien notre ip comme bannie
    ```bash
    [dreasy@db ~]$ sudo fail2ban-client status sshd
    Status for the jail: sshd
    |- Filter
    |  |- Currently failed: 0
    |  |- Total failed:     9
    |  |- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    |- Actions
    |- Currently banned: 1
    |- Total banned:     2
    |- Banned IP list:   10.102.1.11
    ```

### 2. L'évasion !

- Pour révoquer un bannissement sur une adresse ip
    ```bash
    [dreasy@db ~]$ sudo fail2ban-client unban 10.102.1.11
    1
    ```
- Fail2Ban n'affiche plus notre ip comme bannie
    ```bash
    [dreasy@db ~]$ sudo fail2ban-client status sshd
    Status for the jail: sshd
    |- Filter
    |  |- Currently failed: 0
    |  |- Total failed:     9
    |  |- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    |- Actions
    |- Currently banned: 0
    |- Total banned:     2
    |- Banned IP list:
    ```
- On a retrouvé accès à notre machine
    ```bash
    [dreasy@web ~]$ ssh dreasy@10.102.1.12
    dreasy@10.102.1.12's password:
    ```

### Fini la racaille !

![Get banned !](https://media.tenor.com/TbfChfHKkOUAAAAM/ban-button.gif)