# Module 1 : Reverse Proxy

Un reverse proxy est donc une machine que l'on place devant un autre service afin d'accueillir les clients et servir d'intermédiaire entre le client et le service.

L'utilisation d'un reverse proxy peut apporter de nombreux bénéfices :

- décharger le service HTTP de devoir effectuer le chiffrement HTTPS (coûteux en performances)
- répartir la charge entre plusieurs services
- effectuer de la mise en cache
- fournir un rempart solide entre un hacker potentiel et le service et les données importantes
- servir de point d'entrée unique pour accéder à plusieurs services web


## Sommaire

- [Module 1 : Reverse Proxy](#module-1--reverse-proxy)
  - [Sommaire](#sommaire)
- [I. Intro](#i-intro)
- [II. Setup](#ii-setup)
- [III. HTTPS](#iii-https)

# I. Intro

# II. Setup

🖥️ **VM `proxy.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`
- démarrer le service `nginx`

            Complete!
            [dreasy@localhost ~]$ sudo systemctl enable nginx
            Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
            [dreasy@localhost ~]$ sudo systemctl start nginx
            [dreasy@localhost ~]$ sudo systemctl status nginx
            ● nginx.service - The nginx HTTP and reverse proxy server
                Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
                Active: active (running) since Thu 2022-11-17 09:26:44 CET; 6s ago
- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute

      [dreasy@localhost ~]$ ss -ln
        tcp    LISTEN  0       511                                         0.0.0.0:80                   0.0.0.0:*

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX

            [dreasy@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
            success
            [dreasy@localhost ~]$ sudo firewall-cmd --reload
            success
            

- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX

            [dreasy@localhost ~]$ ps -ef |grep nginx
            root        4277       1  0 09:26 ?        00:00:00 nginx: master process /usr/sbin/nginx
            nginx       4278    4277  0 09:26 ?        00:00:00 nginx: worker process
            nginx       4279    4277  0 09:26 ?        00:00:00 nginx: worker process

- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine

    *depuis mon google sur mon os j'ai acces a la page d'accueil de nginx*

➜ **Configurer NGINX**

- nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`

                [dreasy@localhost default.d]$ cat /etc/nginx/nginx.conf

    - procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence

                 # Load configuration files for the default server block.
                include /etc/nginx/default.d/*.conf;

  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`
    
            [dreasy@web ~]$ sudo nano /var/www/tp2_nextcloud/config/config.php
            'trusted_domains' =>
            array (
                0 => '10.102.1.13',
                1 => 'proxy.tp3.linux',
            ),

Référez-vous à monsieur Google pour tout ça :)

Exemple de fichier de configuration minimal NGINX.:

```nginx
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://<IP_DE_NEXTCLOUD>:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal.

            #Dans le fichier de conf de php
              'trusted_domains' =>
                array (
                    0 => 'web.tp2.linux',
                ),

dans le fichier hosts de windows j'ai mis l'ip du proxy
avec le nom de web.tp2.linux
 ```10.102.1.13    web.tp2.linux```

                PS C:\Users\Dreasy> curl web.tp2.linux
            StatusCode        : 200
            StatusDescription : OK
            Content           : <!DOCTYPE html>
            [...]


 Ici on voit que du coup mon windows peut joindre le nextcloud via le proxy mais pas directement le nextcloud donc par le nom de domaine proxy.tp3.linux mais pas par web.tp2.linux.

# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

                sudo firewall-cmd --add-port=443/tcp --permanent
                sudo firewall-cmd --reload
                openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
                33
                ls
                cat /etc/nginx/nginx.conf
                sudo nano /etc/nginx/nginx.conf
                mv server.crt web.tp2.linux.crt
                mv server.key web.tp2.linux.key
                sudo mv web.tp2.linux.crt /etc/pki/tls/certs/
                sudo mv web.tp2.linux.key /etc/pki/tls/private/
                sudo nano /etc/nginx/nginx.conf
                 # Port d'écoute de NGINX
                listen 443 ssl;
                    ssl_certificate /etc/pki/tls/certs/web.tp2.linux.crt;
                    ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;

                sudo systemctl restart nginx
                sudo systemctl status nginx

Dans la config de nextcloud :

                [dreasy@web ~]$ sudo nano /var/www/tp2_nextcloud/config/config.php
                'overwriteprotocol' => 'https',
                'overwrite.cli.url' => 'https://web.tp2.linux',
                'overwritehost' => 'web.tp2.linux',
                'trusted_domains' =>
                array (
                    0 => 'web.tp2.linux',
                ),