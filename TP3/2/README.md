# Module 7 - Fail2Ban

- [Module 7 : Fail2Ban](#module-7--fail2ban)
- [I. Installation de Fail2Ban](#i-installation-de-fail2ban)
- [II. Configuration de Fail2Ban](#ii-configuration-de-fail2ban)
- [III. Testing Zone](#iii-testing-zone)
    - [1. Time to get banned](#1-time-to-get-banned)
    - [2. L'évasion !](#2-lévasion-)

## I. Installation de Fail2Ban

- Installation du repository EPEL
    ```bash
    [dreasy@web ~]$ sudo dnf install epel-release -y
    Complete!
    ```
- Installation de Fail2Ban
    ```bash
    # Installation de Fail2Ban
    [dreasy@web ~]$ sudo dnf install fail2ban -y
    Complete!
    ```

## II. Configuration de Fail2Ban

- On copie la configuration comme demandé
    ```bash
    [dreasy@web ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

    # On peut maintenant modifier jail.local pour configurer Fail2Ban
    ```
- On modifie la configuration
    ```bash
    # Par défaut, Fail2Ban utilise IPtables, donc on modifie ça
    [dreasy@web ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
    [dreasy@web ~]$ sudo systemctl restart fail2ban

    # On crée une conf pour le ssh
    [dreasy@web ~]$ sudo nano /etc/fail2ban/jail.d/sshd.local
    [dreasy@web ~]$ cat /etc/fail2ban/jail.d/sshd.local
    [sshd]
    enabled = true
    bantime = 11111d
    findtime = 1m
    maxretry = 3
    [dreasy@web ~]$ sudo systemctl restart fail2ban

    # On check les status
    [dreasy@web ~]$ sudo fail2ban-client status
    Status
    |- Number of jail:      1
    |- Jail list:   sshd

    # On peut aussi récupérer des propriétés en commandes
    [dreasy@web ~]$ sudo fail2ban-client get sshd maxretry
    3
    ```
- On lance et on active Fail2Ban
    ```bash
    # Lancement du service
    [dreasy@web ~]$ sudo systemctl start fail2ban

    # Activation du service
    [dreasy@web ~]$ sudo systemctl enable fail2ban
    Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
    ```

## III. Testing zone

### 1. Time to get banned

- On essaye de faire 3 erreurs de mots de passe depuis une autre machine
    ```bash
    [dreasy@web ~]$ ssh dreasy@10.102.1.12
    dreasy@10.102.1.12''s password:
    Permission denied, please try again.
    dreasy@10.102.1.12''s password:
    Permission denied, please try again.
    dreasy@10.102.1.12''s password:
    dreasy@10.102.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
    [dreasy@web ~]$ ssh dreasy@10.102.1.12
    ssh: connect to host 10.102.1.12 port 22: Connection refused
    ```
- Fail2Ban affiche bien notre ip comme bannie
    ```bash
    [dreasy@web ~]$ sudo fail2ban-client status sshd
    Status for the jail: sshd
    |- Filter
    |  |- Currently failed: 0
    |  |- Total failed:     9
    |  |- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
    |- Actions
    |- Currently banned: 1
    |- Total banned:     2
    |- Banned IP list:   10.102.1.11
    ```



on peut aussi refaire ce system de protection sur toutes les autres vm du tp pour les protéger des attaques de brute force