# Module 5 - Monitoring

- [Module 5 : Monitoring](#module-5--monitoring)
- [I. Install de netdata](#i-install-de-netdata)
    - [1. Install du service](#1-install-du-service)
    - [2. Lancement de netdata](#2-lancement-de-netdata)
    - [3. Configuration du firewall](#3-configuration-du-firewall)
- [II. Alertes Discord](#ii-alertes-discord)

On mettra ici les détails de l'installation sur la machine `web.tp2.linux`, mais Netdata a été aussi installé sur `db.tp2.linux` pour monitorer tous nos services.

## I. Install de netdata

### 1. Install du service

```bash
# Installation de wget
[dreasy@web ~]$ sudo dnf install wget
Complete!

# Téléchargement de netdata avec kickstart
[dreasy@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
Complete!
```

### 2. Lancement de netdata

```bash
# Lancement du service
[dreasy@web ~]$ sudo systemctl start netdata

# Activation du lancement auto au démarrage de la VM
[dreasy@web ~]$ sudo systemctl enable netdata

# Vérification du statut
[dreasy@web ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
    Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vend>
    Active: active (running) since Thu 2022-11-17 17:08:47 CET; 3min 2s ago
    [...]
```

### 3. Configuration du firewall

```bash
# Récupération du port utilisé par netdata
[dreasy@web ~]$ ss -laptn | grep 19999
LISTEN    0      4096                0.0.0.0:19999              0.0.0.0:*
LISTEN    0      4096                   [::]:19999                 [::]:*

# Ouverture du port utilisé par netdata (port 19999/tcp)
[dreasy@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success

# Rechargement du firewall
[dreasy@web ~]$ sudo firewall-cmd --reload
success
```

## II. Alertes Discord

- Configuration d'un webhook et récupération du lien depuis Discord
- Édition de le config de netstat
    ```bash
    # Ajout du lien webhook dans la config
    [dreasy@web ~]$ sudo /etc/netdata/edit-config health_alarm_notify.conf
    Editing '/etc/netdata/health_alarm_notify.conf' ...
    
    # Création d'une alerte pour l'utilisation du CPU
    [dreasy@web ~]$ sudo touch /etc/netdata/health.d/cpu_usage.conf
    [dreasy@web ~]$ sudo /etc/netdata/edit-config health.d/cpu_usage.conf
    Editing '/etc/netdata/health.d/cpu_usage.conf' ...
    [dreasy@web ~]$ cat /etc/netdata/health.d/cpu_usage.conf
    alarm: cpu_usage
    on: system.cpu
    lookup: average -3s percentage foreach user,system
    units: %
    every: 10s
    warn: $this > 50
    crit: $this > 80
    info: Utilisation du CPU/Systeme/Users

    # Rechargement de netstat
    [dreasy@web ~]$ sudo netdatacli reload-health
    ```

**Testing des altertes Discord**

- Installation de `stress-ng`
    ```bash
    [dreasy@web ~]$ sudo dnf install stress-ng
    Complete!
    ```
- Lancement du stress-test sur le CPU
    ```bash
    [dreasy@web ~]$ stress-ng -c 10 -l 60
    stress-ng: info:  [4346] defaulting to a 86400 second (1 day, 0.00 secs) run per stressor
    stress-ng: info:  [4346] dispatching hogs: 10 cpu
    ```
