# TP3 : Amélioration de la solution NextCloud

## Liste des modules

- [Module 1 - Reverse proxy](./1/README.md)
- [Module 5 - Monitoring](./4/READM.md)
- [Module 6 - Automatiser le déploiement](./3/README.md)
- [Module 7 - Fail2ban](./5/README.md)

    Tps fait avec Luka en coops
    
## Liste des machines utilisées

| Machine           | IP            | Service         |
|-------------------|---------------|-----------------|
|  `web.tp2.linux`  | `10.102.1.11` | Serveur Web     |
|  `db.tp2.linux`   | `10.102.1.12` | Server database |
| `proxy.tp3.linux` | `10.102.1.13` | Serveur proxy   |