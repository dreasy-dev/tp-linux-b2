 #!/bin/sh
 if [[ $EUID -ne 0 ]]; then
     echo "This must be run as root"
     exit 1
 fi
 for port in 7777 7778 27015; do
     firewall-cmd --permanent --add-port $port/udp
     firewall-cmd --permanent --add-port $port/tcp
 done
 #Uncomment the next if you want to open the default rcon port
 #firewall-cmd --permanent --add-port 27020/tcp
 firewall-cmd --reload