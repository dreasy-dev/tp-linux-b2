#! /bin/bash
if [[ $EUID -ne 0 ]]; then
     echo "This must be run as root"
     exit 1
fi
dnf update -y 
dnf upgrade -y 
groupadd docker
usermod -aG docker $USER
echo "fs.file-max=100000" >> /etc/sysctl.conf
echo "success"
echo "*               soft    nofile          1000000" >> /etc/security/limits.conf
echo "success"
echo "*               hard    nofile          1000000" >> /etc/security/limits.conf
echo "success"
echo "session required pam_limits.so" >> /etc/pam.d/common-session
echo "success"
echo "New settings are successfully applied"