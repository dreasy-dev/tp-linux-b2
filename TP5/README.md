# TP5 : Hébergement d'une solution libre et opensource


## Sommaire

- [TP5 : Hébergement d'une solution libre et opensource](#tp5--hébergement-dune-solution-libre-et-opensource)
  - [Sommaire](#sommaire)
  - [Déroulement](#déroulement)
    - [Choix de la solution](#choix-de-la-solution)
    - [Mise en place de la solution](#mise-en-place-de-la-solution)
    - [Maîtrise de la solution](#maîtrise-de-la-solution)
    - [Amélioration de la solution](#amélioration-de-la-solution)
  - [Rendu attendu](#rendu-attendu)

## Déroulement

### Choix de la solution

Pour mon choix de solution j'ai choisi un jeu, ARK c'est un jeu que je connais depuis 7/8 ans maintenant.
je vais juste m'occuper du serveur de jeu ark qu'on peut heberger sur notre ordinateur ou server avec nos settings perso etc...

### Mise en place de la solution

Pour commencer on prend la base d'un rocky linux et on execute le script suivant :

[Server-master.sh ](./files/Server-master.sh)

Ce script va Mettre a jour la machine, installer docker et les outils essentiels pour continuer, et aussi modifier les parametres du noyau pour le docker.



### Maîtrise de la solution

Suite a beaucoup de probleme sur ma solution j'ai pas reussi a simplement la tester en docker. J'ai reussi a la faire fonctionner sur un debian 11 en se connectant dessus depuis le jeu etc... mais je n'ai pas reussi a la faire fonctionner en docker.
en temps normal la solution aurait du etre comme ca :

```bash
docker build . -t Ark-image
docker run -p 27015:27015/udp -p 7777:7777/udp -d --name Ark-server -v ./Firewall-settings.sh:/Firewall-settings.sh -v ./Start-Server.sh:/Start-Server.sh  Ark-image
docker ps -a 
```
Ensuite le serveur est lancé et on peut se connecter dessus depuis le jeu.On peut verifier depuis steam plus simplement si le serveur communique bien avec steam, le monde, et notre client.

### Amélioration de la solution

Cette partie dépendra beaucoup de la solution que vous avez retenu. Pensez comme au TP3.

Chaque brique peut-être améliorée, d'un point de vue sécurité, performances, ou facilité la maintenabilité. 

Etant donné que j'ai pas reussi a le faire fonctionner correctement en docker, je n'ai pas pu ameliorer la solution.
Mais je sais ce qui aurait du etre fait pour ameliorer la solution :
    - On aurait pu ajouter fail2ban pour securiser le serveur, 
    - Un reverse proxy pour le rendre plus securisé et plus facile a gerer.
    - La mise a jour du jeu se fait au demarrage du docker quand le steamcmd se lance, 
    - j'aurai pu rajouter peut etre une interface web pour gerer le serveur et avoir toutes les informations en live histoire d'avoir une solution bien plus simplifiée et userfriendly.

## Rendu attendu


- Un fichier README.md qui explique le choix de la solution, le déroulement du TP, et les améliorations possibles.
- Un fichier Dockerfile qui permet de construire l'image docker de la solution.

# TP5 : Hébergement d'une solution libre et opensource

